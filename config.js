const dotenv = require("dotenv").config();

module.exports = {
  port: process.env.PORT,
  shortcode: process.env.SHORT_CODE,
  serverurl: process.env.SERVER_URL,
  passkey: process.env.PASS_KEY,
  securitycredential: process.env.SECURITY_CREDENTIAL,
  initiatorname: process.env.INITIATOR_NAME
};
