require("rootpath")();
const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const errorHandler = require("_helpers/error-handler");
const { access } = require("./auth/accesstoken");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const corsOptions = {
  origin: "*"
};
app.use(cors(corsOptions));
const request = require("request");
const {
  shortcode,
  passkey,
  serverurl,
  securitycredential,
  port,
  initiatorname
} = require("./config");


// Routes
app.get("/", (req, res) => {
  res.send("<h1>Welcome to safaricom Daraja API </h1>");
});
// Access Tokens
app.get("/access_token", access, (req, res) => {
  res.status(200).json({ access_token: req.access_token });
});
// Register
app.get("/register", access, (req, res) => {
  const url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl";
  const auth = "Bearer " + req.access_token;

  request(
    {
      url: url,
      method: "POST",
      headers: { Authorization: auth },
      json: {
        ShortCode: shortcode,
        ResponseType: "Completed",
        ConfirmationURL: `https://${serverUrl}/confirmation`,
        ValidationURL: `https://${serverUrl}/validation`
      }
    },
    (error, response, body) => {
      if (error) {
        console.log(error);
      }
      res.status(200).json(body);
    }
  );
});

app.post("/confirmation", (req, res) => {
  console.log(".............confirmation.................");
  console.log(req.body);
  res.status(200).json(req.body);
});

app.post("/validation", (req, res) => {
  console.log(".............validation.................");
  console.log(req.body);
  res.status(200).json(req.body);
});
// Customer to Business
app.get("/simulate", access, (req, res) => {
  const url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate";
  const auth = "Bearer " + req.access_token;
  request(
    {
      url: url,
      method: "POST",
      headers: {
        Authorization: auth
      },
      json: {
        CommandID: "CustomerPayBillOnline",
        Amount: "10",
        Msisdn: "254708374149",
        BillRefNumber: "TestAPI",
        ShortCode: shortcode
      }
    },
    (error, response, body) => {
      if (error) {
        console.log(error);
      }
      res.status(200).json(body);
    }
  );
});
// Stk request phone to pay
app.get("/stk", access, (req, res) => {
  const url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest";
  const auth = "Bearer " + req.access_token;

  let date = new Date();
  let currentDate = ("0" + date.getDate()).slice(-2);
  let currentHours = ("0" + date.getHours()).slice(-2);
  let currentMinutes = ("0" + date.getMinutes()).slice(-2);
  let currentSeconds = ("0" + date.getSeconds()).slice(-2);
  let currentMonth = ("0" + (date.getMonth() + 1)).slice(-2);
  const timestamp =
    date.getFullYear() +
    "" +
    "" +
    currentMonth +
    "" +
    "" +
    currentDate +
    "" +
    "" +
    currentHours +
    "" +
    "" +
    currentMinutes +
    "" +
    "" +
    currentSeconds;

  const password = new Buffer.from("174379" + passkey + timestamp).toString(
    "base64"
  );

  request(
    {
      url: url,
      method: "POST",
      headers: {
        Authorization: auth
      },
      json: {
        BusinessShortCode: "174379",
        Password: password,
        Timestamp: timestamp,
        TransactionType: "CustomerPayBillOnline",
        Amount: req.query.amount,
        PartyA: "254708374149",
        PartyB: "174379",
        PhoneNumber: req.query.phonenumber,
        CallBackURL: `https://${serverurl}/stk_callback`,
        AccountReference: "CompanyXLTD",
        TransactionDesc: "Payment of X"
      }
    },
    (error, response, body) => {
      if (error) {
        console.log(error);
      }
      res.status(200).json(body);
    }
  );
});

//STK response
app.post("/stk_callback", (req, res) => {
  console.log(".......... STK Callback ..................");
  console.log(JSON.stringify(req.body));
});

// Business to Transaction
app.get("/b2c", access, (req, res) => {
  const url = "https://sandbox.safaricom.co.ke/mpesa/b2c/v1/paymentrequest";
  const auth = "Bearer " + req.access_token;

  request(
    {
      url: url,
      method: "POST",
      headers: {
        Authorization: auth
      },
      json: {
        InitiatorName: initiatorname,
        SecurityCredential: securitycredential,
        CommandID: "BusinessPayment",
        Amount: "1",
        PartyA: "600981",
        PartyB: "254708374149",
        Remarks: "Test remarks ",
        QueueTimeOutURL: `https://${serverurl}/b2c/queue`,
        ResultURL: `https://${serverurl}/b2c/result`,
        Occassion: "Sent Wrongly"
      }
    },
    (error, response, body) => {
      if (error) {
        console.log(error);
      }
      res.status(200).json(body);
    }
  );
});

//B2C REsponse
app.post("/b2c/result", (req, res) => {
  console.log("-------------------- B2C Result -----------------");
  console.log(JSON.stringify(req.body.Result));
});

app.post("/b2c/queue", (req, res) => {
  console.log("-------------------- B2C Timeout -----------------");
  console.log(req.body);
});

// global error handler
app.use(errorHandler);

// start server
const p0rt = port;
const server = app.listen(p0rt, function () {
  console.log(`Server listening on: http://localhost:${p0rt}`);
});
